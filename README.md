# PHP Simple Authentication

## Installation

1. Setup Apache document root to `phpintro/dist/`
1. Install NPM dependencies: `npm i` / `yarn`
1. Install PHP: `composer install`
1. Run `gulp`

